<?php

namespace Drupal\vb_menu;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\menu_link_content\MenuLinkContentAccessControlHandler;

/**
 * Defines the access control handler for the menu link content entity type.
 */
class VbMenuAccessControlHandler extends MenuLinkContentAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $access = parent::checkAccess($entity, $operation, $account);

    if ($operation == 'update' || $operation = 'delete') {
      if ($entity->hasField('field_block') && $entity->field_block->getValue()) {
        $access = AccessResult::allowedIfHasPermission($account, $operation . ' menu block');
      }
      if ($entity->hasField('field_view') && $entity->field_view->getValue()) {
        $access = AccessResult::allowedIfHasPermission($account, $operation . ' menu view');
      }
      if ($entity->hasField('field_node') && $entity->field_node->getValue()) {
        $access = AccessResult::allowedIfHasPermission($account, $operation . ' menu node');
      }
    }

    return $access;
  }

}
