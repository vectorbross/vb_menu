<?php

namespace Drupal\vb_menu\Controller;

use Drupal\menu_link_content\Controller\MenuController;
use Drupal\system\MenuInterface;

/**
 * Defines a route controller for a form for menu link content entity creation.
 */
class VbMenuController extends MenuController {

  /**
   * Provides the menu block creation form.
   *
   * @param \Drupal\system\MenuInterface $menu
   *   An entity representing a custom menu.
   *
   * @return array
   *   Returns the menu block creation form.
   */
  public function addBlock(MenuInterface $menu) {
    $menu_link = $this->entityTypeManager()
      ->getStorage('menu_link_content')
      ->create([
        'menu_name' => $menu->id(),
      ]);
    return $this->entityFormBuilder()->getForm($menu_link, 'block');
  }

  /**
   * Provides the menu view creation form.
   *
   * @param \Drupal\system\MenuInterface $menu
   *   An entity representing a custom menu.
   *
   * @return array
   *   Returns the menu view creation form.
   */
  public function addView(MenuInterface $menu) {
    $menu_link = $this->entityTypeManager()
      ->getStorage('menu_link_content')
      ->create([
        'menu_name' => $menu->id(),
      ]);
    return $this->entityFormBuilder()->getForm($menu_link, 'view');
  }

  /**
   * Provides the menu node creation form.
   *
   * @param \Drupal\system\MenuInterface $menu
   *   An entity representing a custom menu.
   *
   * @return array
   *   Returns the menu node creation form.
   */
  public function addNode(MenuInterface $menu) {
    $menu_link = $this->entityTypeManager()
      ->getStorage('menu_link_content')
      ->create([
        'menu_name' => $menu->id(),
      ]);
    return $this->entityFormBuilder()->getForm($menu_link, 'node');
  }

}
